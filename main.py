# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Training and evaluation"""

import run_lib
from absl import app
from absl import flags
from utils import Wandb
from ml_collections.config_flags import config_flags
import tensorflow as tf
import logging
import os


FLAGS = flags.FLAGS

config_flags.DEFINE_config_file(
  "config", None, "Training configuration.", lock_config=True)
flags.DEFINE_string("workdir", None, "Work directory.")
flags.DEFINE_bool("wandb", False, "launch wandb or not")
flags.DEFINE_enum("mode", None, ["train", "sampling", "fid", "fid_stats"], "Running mode")
flags.DEFINE_string("eval_folder", "eval",
                    "The folder name for storing evaluation results")

flags.DEFINE_string("ckpt", None, "sampling mode ckpt")
flags.DEFINE_string("result_folder", None, "sampling result folder")
flags.mark_flags_as_required(["config", "mode"])

def resolve_result_folder(config, ckpt_path):
  sampler_name = config.sampling.method
  if sampler_name.lower() == 'gddim':
    info_path=f"gddim_order{config.sampling.gddim_order}_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  if sampler_name.lower() == 'sgddim':
    if not config.sampling.sgddim_use_order0 or config.sampling.gddim_order>0:
      info_path=f"sgddim_lambda{config.sampling.lambda_coef}_order{config.sampling.gddim_order}_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    else:
      info_path=f"sgddim_order0_lambda{config.sampling.lambda_coef}_order{config.sampling.gddim_order}_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  elif sampler_name.lower() == 'lgddim':
    info_path=f"Lgddim_order{config.sampling.gddim_order}_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  elif sampler_name.lower() == 'sscs':
    info_path=f"sscs_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  elif sampler_name.lower() == 'em':
    info_path=f"em_lambda{config.sampling.lambda_coef}_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  elif sampler_name.lower() == 'mlgddim':
    info_path=f"mlgddim_order{config.sampling.gddim_order}_nfe{config.sampling.nfe}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  elif sampler_name.lower() == 'hybgddim':
    info_path=f"hybgddim_order{config.sampling.gddim_order}_nfe{config.sampling.nfe}_nfer{config.sampling.noise_nfe_ratio}_imgtr{config.sampling.img_t_ratio}_ts{config.sampling.ts_order}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  elif sampler_name.lower() == 'ode':
    info_path=f"ode_atol{config.sampling.atol}_rtol{config.sampling.rtol}_{config.sampling.ode_method}{'denoising' if config.sampling.noise_removal else ''}"
    return os.path.join(f"{ckpt_path}_eval", info_path)
  else:
    raise RuntimeError(f"{sampler_name} not supported")

def main(argv):
  os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

  if FLAGS.mode == "train":
    tf.config.experimental.set_visible_devices([], "GPU")
    Wandb.name = f"{FLAGS.workdir[5:]}-{FLAGS.mode}"
    Wandb.ready_launch = FLAGS.wandb

    tf.config.experimental.set_visible_devices([], "GPU")
    # Create the working directory
    tf.io.gfile.makedirs(FLAGS.workdir)
    run_lib.launch_wandb(FLAGS.config,FLAGS.workdir)
    # Set logger so that it outputs to both console and file
    # Make logging work for both disk and Google Cloud Storage
    gfile_stream = tf.io.gfile.GFile(os.path.join(FLAGS.workdir, 'stdout.txt'), 'w')
    handler = logging.StreamHandler(gfile_stream)
    formatter = logging.Formatter('%(levelname)s - %(filename)s - %(asctime)s - %(message)s')
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(handler)
    logger.setLevel('INFO')
    # Run the training pipeline
    run_lib.train(FLAGS.config, FLAGS.workdir)
  elif FLAGS.mode == "sampling":
    result_folder = FLAGS.result_folder if FLAGS.result_folder else resolve_result_folder(FLAGS.config, FLAGS.ckpt)
    run_lib.sample_data(FLAGS.config, FLAGS.ckpt, result_folder)
  elif FLAGS.mode == "fid":
    result_folder = FLAGS.result_folder if FLAGS.result_folder else resolve_result_folder(FLAGS.config, FLAGS.ckpt)
    run_lib.check_fid(FLAGS.config, result_folder)
  elif FLAGS.mode == "fid_stats":
    run_lib.fid_stats(FLAGS.config)
  else:
    raise ValueError(f"Mode {FLAGS.mode} not recognized.")


if __name__ == "__main__":
  app.run(main)
