# <p align="center">gDDIM: Generalized denoising diffusion implicit models</p>

<div align="center">
  <a href="https://qsh-zh.github.io/" target="_blank">Qinsheng&nbsp;Zhang</a> &emsp; <b>&middot;</b> &emsp;
  <a href="https://mtao8.math.gatech.edu/" target="_blank">Molei&nbsp;Tao</a> &emsp; <b>&middot;</b> &emsp;
  <a href="https://yongxin.ae.gatech.edu/" target="_blank">Yongxin&nbsp;Chen</a>
  <br> <br>
  <a href="arxiv.org/abs/2204.13902" target="_blank">Paper</a> &emsp;
</div>
<br><br>

gDDIM accelerates general diffusion models besides isotropic diffusions. 
We unbox the accelerating secret of DDIMs based on Dirac approximation and generalize it to general diffusion models. 
Empirically, we validate gDDIM in two non-isotropic diffusions, [Blurring diffusion model]() and [Critically-sampled Langevin diffusion model]().

![gDDIM](assets/fig1.png) 
![dirac](assets/fig2.png)

# Setup and Reproduce 

We recommend [docker](https://www.docker.com/) to set up environment. Here is [Dockerfile](Dockerfile) and [docker image](https://hub.docker.com/repository/docker/qinsheng/gddim/general), which contains necessary packages. The codebase is in Nvidia A6000 and V100. 

## gDDIM with toy data

```shell
python main.py --config configs/default_points_config.py --mode train --workdir logs/2d_point --wandb
```

[wandb run](https://wandb.ai/qinsheng/gddim_pub)

## gDDIM with CIFAR10

```shell
# training
python main.py --config configs/cifar10_config.py --mode train --workdir logs/cifar --config.seed=123

# generate samples and save in folder $result_folder
python main.py --config --config configs/cifar10_config.py --mode sampling --result_folder $result_folder --ckpt $ckpt_path

# evaluate fid
python main.py --config --config configs/cifar10_config.py --mode fid --result_folder $result_folder
```

To evaluate FID, you need download [`cifar10_stats.npz`](https://drive.google.com/file/d/1fXgBupLzThTGLLsiYCHRQJixuDsR1bSI/view?usp=sharing) ans save it to `assets/stats/`. [Yang Song's score_sde](https://github.com/yang-song/score_sde#stats-files-for-quantitative-evaluation) includes more instructions.

The [checkpoint]() reached 2.2565 FID with 50 NFE.


# Reference

```tex
@inproceedings{zhang2022gddim,
      title={gDDIM: Generalized denoising diffusion implicit models}, 
      author={Qinsheng Zhang and Molei Tao and Yongxin Chen},
      booktitle={International Conference on Learning Representations (ICLR)},
      year={2023}
}
```

Related works

```tex
@inproceedings{song2020denoising,
  title={Denoising diffusion implicit models},
  author={Song, Jiaming and Meng, Chenlin and Ermon, Stefano},
  booktitle={International Conference on Learning Representations (ICLR)},
  year={2021}
}

@inproceedings{dockhorn2022score,
    title={Score-Based Generative Modeling with Critically-Damped Langevin Diffusion},
    author={Tim Dockhorn and Arash Vahdat and Karsten Kreis},
    booktitle={International Conference on Learning Representations (ICLR)},
    year={2022}
}

@article{hoogeboom2022blurring,
  title={Blurring diffusion models},
  author={Hoogeboom, Emiel and Salimans, Tim},
  journal={arXiv preprint arXiv:2209.05557},
  year={2022}
}
```
